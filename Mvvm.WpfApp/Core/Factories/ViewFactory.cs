﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Autofac;
using Mvvm.WpfApp.Core.ViewModels;
using Mvvm.WpfApp.ViewModels;

namespace Mvvm.WpfApp.Core.Factories
{
    public class ViewFactory : IViewFactory
    {
        private readonly IDictionary<Type, Type> _map = new Dictionary<Type, Type>();
        private readonly IComponentContext _dependencyResolver;

        public ViewFactory(IComponentContext dependencyResolver)
        {
            _dependencyResolver = dependencyResolver;
        }

        public void Register<TViewModel, TView>()
            where TViewModel : class, IViewModel
            where TView : Page
        {
            _map[typeof(TViewModel)] = typeof(TView);
        }

        public Page Resolve<TViewModel>(Action<TViewModel> setStateAction = null)
            where TViewModel : class, IViewModel
        {
            TViewModel viewModel;
            var view = Resolve(out viewModel, setStateAction);
            viewModel.Locator = _dependencyResolver.Resolve<ViewModelLocator>();
            return view;
        }

        public Page Resolve<TViewModel>(out TViewModel viewModel, Action<TViewModel> setStateAction = null)
            where TViewModel : class, IViewModel
        {
            viewModel = _dependencyResolver.Resolve<TViewModel>();
            viewModel.Locator = _dependencyResolver.Resolve<ViewModelLocator>();

            var viewType = _map[typeof(TViewModel)];
            var view = _dependencyResolver.Resolve(viewType) as Page;

            if (setStateAction != null)
                viewModel.SetState(setStateAction);

            if (view != null)
                view.DataContext = viewModel;

            return view;
        }

        public Page Resolve<TViewModel>(TViewModel viewModel)
            where TViewModel : class, IViewModel
        {
            var viewType = _map[typeof(TViewModel)];
            var view = _dependencyResolver.Resolve(viewType) as Page;

            if (view != null)
                view.DataContext = viewModel;

            return view;
        }
    }
}