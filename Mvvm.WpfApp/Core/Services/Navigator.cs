﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Navigation;
using Mvvm.WpfApp.Core.Factories;
using Mvvm.WpfApp.Core.ViewModels;

namespace Mvvm.WpfApp.Core.Services
{
    public class Navigator : INavigator
    {
        private readonly Lazy<NavigationWindow> _firstWindow;
        private readonly Stack<NavigationWindow> _windows;
        private readonly IViewFactory _viewFactory;

        private NavigationWindow TopWindow
        {
            get
            {
                if (_windows.Count == 0)
                    _windows.Push(_firstWindow.Value);

                return _windows.Peek();
            }
        }


        public Navigator(Lazy<NavigationWindow> window, IViewFactory viewFactory)
        {
            _viewFactory = viewFactory;
            _firstWindow = window;
            _windows = new Stack<NavigationWindow>();
        }

        public TViewModel GoBack<TViewModel>()
            where TViewModel : class, IViewModel
        {
            if (TopWindow.CanGoBack == false)
            {
                if (_windows.Count <= 1)
                    return null;

                TopWindow.Close();
            }
            else
            {
                TopWindow.GoBack();
            }

            var page = TopWindow.Content as Page;
            if (page == null)
                return null;

            TViewModel viewModel = page.DataContext as TViewModel;
            if (viewModel != null)
                TopWindow.Title = viewModel.Title ?? "";

            return viewModel;
        }

        public TViewModel Navigate<TViewModel>(Action<TViewModel> setStateAction = null)
            where TViewModel : class, IViewModel
        {
            TViewModel viewModel;
            var page = _viewFactory.Resolve(out viewModel, setStateAction);

            TopWindow.Navigate(page);
            TopWindow.Title = viewModel.Title ?? "";

            return viewModel;
        }

        public TViewModel Navigate<TViewModel>(TViewModel viewModel)
            where TViewModel : class, IViewModel
        {
            var page = _viewFactory.Resolve(viewModel);

            TopWindow.Navigate(page);
            TopWindow.Title = viewModel.Title ?? "";

            return viewModel;
        }

        public TViewModel NavigateModal<TViewModel>(Action<TViewModel> setStateAction = null)
            where TViewModel : class, IViewModel
        {
            TViewModel viewModel;
            var page = _viewFactory.Resolve(out viewModel, setStateAction);

            var newWindow = new NavigationWindow
            {
                Owner = TopWindow,
                Width = page.Width,
                Height = page.Height,
                Title = viewModel.Title ?? ""
            };
            newWindow.Closed += RemoveTopWindowFromStack;

            _windows.Push(newWindow);

            newWindow.Navigate(page);
            newWindow.ShowDialog();

            return viewModel;
        }

        public TViewModel NavigateModal<TViewModel>(TViewModel viewModel)
            where TViewModel : class, IViewModel
        {
            var page = _viewFactory.Resolve(viewModel);

            var newWindow = new NavigationWindow
            {
                Owner = TopWindow,
                Width = page.Width,
                Height = page.Height,
                Title = viewModel.Title ?? ""
            };
            newWindow.Closed += RemoveTopWindowFromStack;

            _windows.Push(newWindow);

            newWindow.Navigate(page);
            newWindow.ShowDialog();

            return viewModel;
        }

        private void RemoveTopWindowFromStack(object sender, EventArgs e)
        {
            var window = _windows.Pop();

            if (window != null)
                window.Closed -= RemoveTopWindowFromStack;
        }
    }
}