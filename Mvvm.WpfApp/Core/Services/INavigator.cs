﻿using System;
using Mvvm.WpfApp.Core.ViewModels;

namespace Mvvm.WpfApp.Core.Services
{
    public interface INavigator
    {
        TViewModel GoBack<TViewModel>()
            where TViewModel : class, IViewModel;

        TViewModel Navigate<TViewModel>(Action<TViewModel> setStateAction = null)
            where TViewModel : class, IViewModel;

        TViewModel Navigate<TViewModel>(TViewModel viewModel)
            where TViewModel : class, IViewModel;

        TViewModel NavigateModal<TViewModel>(Action<TViewModel> setStateAction = null)
            where TViewModel : class, IViewModel;

        TViewModel NavigateModal<TViewModel>(TViewModel viewModel)
            where TViewModel : class, IViewModel;
    }
}