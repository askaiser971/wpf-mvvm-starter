﻿using System;
using GalaSoft.MvvmLight;

namespace Mvvm.WpfApp.Core.ViewModels
{
    public class CustomViewModelBase : ViewModelBase, IViewModel
    {
        /// <summary>
        /// ViewModelLocator du projet, automatique injecté par la ViewFactory.
        /// Permet à la vue du ViewModel actuellement édité d'accéder à d'autres ViewModels
        /// sans directement avoir de référence vers ceux-ci.
        /// À utiliser  pour accéder à un ViewModel statique, comme celui d'un menu, par exemple.
        /// </summary>
        public object Locator { get; set; }

        /// <summary>
        /// Titre de la vue associée.
        /// Automatique défini par la ViewFactory
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Permet de faire transiter des données entre deux ViewModel pendant la navigation.
        /// </summary>
        public void SetState<T>(Action<T> action)
            where T : class, IViewModel
        {
            action(this as T);
        }
    }
}
