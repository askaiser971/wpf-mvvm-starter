﻿using System;

namespace Mvvm.WpfApp.Core.ViewModels
{
    public interface IViewModel
    {
        object Locator { get; set; }

        string Title { get; set; }

        void SetState<T>(Action<T> action)
            where T : class, IViewModel;
    }
}