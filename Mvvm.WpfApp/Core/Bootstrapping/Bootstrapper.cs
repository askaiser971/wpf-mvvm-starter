﻿using System.Windows.Navigation;
using Autofac;
using Mvvm.WpfApp.Core.Factories;
using Mvvm.WpfApp.ViewModels;
using Mvvm.WpfApp.Views;

namespace Mvvm.WpfApp.Core.Bootstrapping
{
    public class Bootstrapper : AutofacBootstrapper
    {
        private readonly App _application;

        public Bootstrapper(App application)
        {
            _application = application;
        }

        protected override void ConfigureContainer(ContainerBuilder builder)
        {
            base.ConfigureContainer(builder);
            builder.RegisterModule<MvvmDependencyModule>();
        }

        protected override void RegisterViews(IViewFactory viewFactory)
        {
            viewFactory.Register<FirstViewModel, FirstView>();
            viewFactory.Register<SecondViewModel, SecondView>();
            viewFactory.Register<ThirdViewModel, ThirdView>();
        }

        protected override void ConfigureApplication(IContainer container)
        {
            var viewFactory = container.Resolve<IViewFactory>();
            FirstViewModel viewModel;
            var mainPage = viewFactory.Resolve(out viewModel);

            var window = new NavigationWindow
            {
                Title = viewModel.Title ?? "",
                Width = mainPage.Width,
                Height = mainPage.Height
            };

            window.Navigate(mainPage);
            window.Show();

            _application.MainWindow = window;
        }
    }
}
