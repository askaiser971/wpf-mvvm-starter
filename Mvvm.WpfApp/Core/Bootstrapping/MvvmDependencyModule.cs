﻿using Autofac;
using Mvvm.WpfApp.ViewModels;
using Mvvm.WpfApp.Views;

namespace Mvvm.WpfApp.Core.Bootstrapping
{
    /// <summary>
    /// Enregistre les dépendences dans le système d'IoC.
    /// Voir la classe <see cref="Bootstrapper"/> pour l'association des ViewModels et Views.
    /// </summary>
    public class MvvmDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // ViewModels
            builder.RegisterType<ViewModelLocator>().SingleInstance();
            builder.RegisterType<FirstViewModel>().SingleInstance();
            builder.RegisterType<SecondViewModel>().SingleInstance();
            builder.RegisterType<ThirdViewModel>().SingleInstance();

            builder.RegisterType<MenuViewModel>().SingleInstance();

            // Views
            builder.RegisterType<FirstView>().SingleInstance();
            builder.RegisterType<SecondView>().SingleInstance();
            builder.RegisterType<ThirdView>().SingleInstance();
        }
    }
}
