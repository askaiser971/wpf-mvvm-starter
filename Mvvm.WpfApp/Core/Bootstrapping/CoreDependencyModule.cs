﻿using System.Windows;
using System.Windows.Navigation;
using Autofac;
using Mvvm.WpfApp.Core.Factories;
using Mvvm.WpfApp.Core.Services;

namespace Mvvm.WpfApp.Core.Bootstrapping
{
    public class CoreDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // service registration
            builder.RegisterType<ViewFactory>()
                .As<IViewFactory>()
                .SingleInstance();

            builder.RegisterType<Navigator>()
                .As<INavigator>()
                .SingleInstance();

            // navigation registration
            builder.Register(context =>
                Application.Current.MainWindow as NavigationWindow
            ).SingleInstance();
        }
    }
}