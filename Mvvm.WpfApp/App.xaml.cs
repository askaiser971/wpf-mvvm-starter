﻿using System.Windows;
using Mvvm.WpfApp.Core.Bootstrapping;

namespace Mvvm.WpfApp
{
    public partial class App
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            new Bootstrapper(this).Run();
        }
    }
}
