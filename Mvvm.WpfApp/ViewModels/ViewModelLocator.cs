﻿using Autofac;

namespace Mvvm.WpfApp.ViewModels
{
    /// <summary>
    /// Classe injectée dans chaque ViewModel descendant de CustomViewModelBase.
    /// </summary>
    public class ViewModelLocator
    {
        private readonly IComponentContext _dependencyResolver;

        public ViewModelLocator(IComponentContext dependencyResolver)
        {
            _dependencyResolver = dependencyResolver;
        }

        public MenuViewModel Menu
        {
            get { return _dependencyResolver.Resolve<MenuViewModel>(); }
        }
    }
}
