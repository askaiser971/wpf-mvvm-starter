﻿using GalaSoft.MvvmLight.Command;
using Mvvm.WpfApp.Core.Services;
using Mvvm.WpfApp.Core.ViewModels;

namespace Mvvm.WpfApp.ViewModels
{
    public class FirstViewModel : CustomViewModelBase
    {
        private readonly INavigator _navigator;

        public RelayCommand GoToSecondViewCommand { get; private set; }


        public FirstViewModel(INavigator navigator)
        {
            _navigator = navigator;

            Title = "Première fenêtre";
            GoToSecondViewCommand = new RelayCommand(GoToSecondView);
        }

        public void GoToSecondView()
        {
            _navigator.NavigateModal<SecondViewModel>();
        }
    }
}
