﻿using Mvvm.WpfApp.Core.ViewModels;

namespace Mvvm.WpfApp.ViewModels
{
    public class ThirdViewModel : CustomViewModelBase
    {
        public string MonTexte { get; set; }

        public ThirdViewModel()
        {
            Title = "Ma troisième vue";
            MonTexte = "Hello";
        }
    }
}
