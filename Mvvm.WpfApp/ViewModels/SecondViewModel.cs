﻿using GalaSoft.MvvmLight.Command;
using Mvvm.WpfApp.Core.Services;
using Mvvm.WpfApp.Core.ViewModels;

namespace Mvvm.WpfApp.ViewModels
{
    public class SecondViewModel : CustomViewModelBase
    {
        private readonly INavigator _navigator;

        public RelayCommand GoToFirstViewCommand { get; private set; }

        public RelayCommand GoToThirdViewCommand { get; private set; }


        public SecondViewModel(INavigator navigator)
        {
            _navigator = navigator;

            Title = "Deuxième fenêtre";
            GoToFirstViewCommand = new RelayCommand(GoToFirstView);
            GoToThirdViewCommand = new RelayCommand(GoToThirdView);
        }

        private void GoToThirdView()
        {
            _navigator.Navigate<ThirdViewModel>();
        }

        private void GoToFirstView()
        {
            _navigator.GoBack<FirstViewModel>();
        }
    }
}
