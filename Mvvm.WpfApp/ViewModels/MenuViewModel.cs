﻿using System.Windows;
using GalaSoft.MvvmLight.Command;

namespace Mvvm.WpfApp.ViewModels
{
    public class MenuViewModel
    {
        public RelayCommand SayHelloCommand { get; private set; }

        private bool _canSayHello = true;

        public MenuViewModel()
        {
            SayHelloCommand = new RelayCommand(SayHello, CanSayHello);
        }

        private bool CanSayHello()
        {
            return _canSayHello;
        }

        public void SayHello()
        {
            _canSayHello = false;
            SayHelloCommand.RaiseCanExecuteChanged();
            MessageBox.Show("Hello");
        }
    }
}
